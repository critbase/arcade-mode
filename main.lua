meta.name = "arcade mode"
meta.version = "2.3.0"
meta.description = 'spelunky forever'
meta.author = 'critbase'

--Initial variables
local total_level_count = 1
local level_count = 1
local world_count = 1
local theme = THEME.DWELLING
--curated list of level types,
--because you wouldn't want to fight olmec 4 times in a row... right?
local themelist = {
  THEME.DWELLING,
  THEME.JUNGLE,
  THEME.VOLCANA,
  THEME.TIDE_POOL,
  THEME.TEMPLE,
  THEME.ICE_CAVES, -- TODO: only one ice cave level
  THEME.NEO_BABYLON,
  THEME.SUNKEN_CITY
}

-- since Abzu, Duat, and City of Gold could theoretically be reached even out of order
-- (given enough time and patience lol) i'm not going to include those here
local function random_theme()
  -- 10% chance for getting olmec
  if (math.random(1, 100) < 10) then
    return THEME.OLMEC
    -- technically since eggplant world is accessible too, that shouldn't be here either
  else
    return themelist[math.random(#themelist)]
  end
end
--reset function, used only when runs restart
local function reset_level_count()
  total_level_count = 1
  level_count = 1
  world_count = 1
  theme = random_theme()
end

--increment function called only once every level
--picks the next theme only once if its time
local function increment_level()
  --every 4th level, change to a different theme
  if (level_count % 4) == 0 then
    theme = random_theme()
    world_count = world_count + 1
    level_count = 1
  elseif (
        (state.theme_next == THEME.ICE_CAVES) or
        (state.theme_next == THEME.CITY_OF_GOLD) or
        (state.theme_next == THEME.OLMEC) or
        (state.theme_next == THEME.ABZU) or
        (state.theme_next == THEME.DUAT) or
        (state.theme_next == THEME.TIAMAT) or
        (state.theme_next == THEME.EGGPLANT_WORLD)
      )
  then
    theme = random_theme()
    world_count = world_count + 1
    level_count = 1
  elseif (state.theme == THEME.SUNKEN_CITY) and (state.level == 3) then
    if (math.random(1, 100) < 30) then
      state.theme_next = THEME.HUNDUN
    end
    level_count = level_count + 1
  elseif (state.theme == THEME.NEO_BABYLON) and (state.level == 3) then
    if (math.random(1, 100) < 30) then
      state.theme_next = THEME.TIAMAT
    end
    level_count = level_count + 1
  else
    level_count = level_count + 1
  end
  total_level_count = total_level_count + 1
end

--perpetual world 1
local function settheme()
  state.world_next = world_count
  state.level_next = level_count
  state.theme_next = theme
end

---@param render_ctx VanillaRenderContext
local function render_total_levels(render_ctx, hud)
  local text = "current level: " .. tostring(total_level_count - 1)
  local scale = 0.0007
  render_ctx.draw_text(render_ctx, text, -0.28, 0.9, scale, scale, Color:white(), VANILLA_TEXT_ALIGNMENT.CENTER,
    VANILLA_FONT_STYLE.NORMAL)
end

local function replace_tiamat_hundun_doors()
  if #state.level_gen.exit_doors > 0 then
    local doors = get_entities_by(ENT_TYPE.FLOOR_DOOR_EXIT, MASK.FLOOR, LAYER.BOTH)
    for _, v in ipairs(doors) do
      set_door_target(v, state.world_next, state.level_next, state.theme_next)
    end
  end

  -- check if we have two or more exit doors; in that case, we want to make their destinations different
  if #state.level_gen.exit_doors > 1 then
    local doors = get_entities_by(ENT_TYPE.FLOOR_DOOR_EXIT, MASK.FLOOR, LAYER.BOTH)
    for i, v in ipairs(doors) do
      if i == 0 then
        set_door_target(v, state.world_next, state.level_next, state.theme_next)
      else
        set_door_target(v, state.world_next, state.level_next, random_theme())
      end
    end
  end
end

set_callback(increment_level, ON.LEVEL)
set_callback(render_total_levels, ON.RENDER_PRE_HUD)
set_callback(replace_tiamat_hundun_doors, ON.LEVEL)

--gonna be honest, idk which of these callbacks are even necessary
set_callback(settheme, ON.LEVEL)
set_callback(settheme, ON.LOADING)
set_callback(settheme, ON.POST_LEVEL_GENERATION)

set_callback(reset_level_count, ON.RESET)
set_callback(reset_level_count, ON.DEATH)
